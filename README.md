### Task 9 - Luhn Algorithm

## Description

Command Line App that takes an input of numbers of any length.

It uses the Luhn Algorithm to perform a simple checksum on the last digit of the user input.