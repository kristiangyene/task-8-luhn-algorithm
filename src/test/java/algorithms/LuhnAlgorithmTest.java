package algorithms;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgorithmTest {

    LuhnAlgorithm luhn = new LuhnAlgorithm();

    @Test
    void checkSum() {
        assertEquals("Valid", luhn.checkSum("2", "2"));
        assertEquals("Not valid", luhn.checkSum("1", "5"));
        assertNotEquals("Valid", luhn.checkSum("2", "3"));
        assertNotEquals("Not valid", luhn.checkSum("3", "3"));
    }

    @Test
    void calculateLuhn() {
        assertEquals("2", luhn.calculateLuhn("424242424242424"));
        assertEquals("7", luhn.calculateLuhn("134943744343448"));
        assertNotEquals("3", luhn.calculateLuhn("6546765443545"));
        assertNotEquals("1", luhn.calculateLuhn("18288394565944"));
    }

    @Test
    void validateCreditCard() {
        assertTrue(luhn.validateCreditCard("4565458325965845"));
        assertTrue(luhn.validateCreditCard("4738278577685457"));
        assertFalse(luhn.validateCreditCard("456545859658453"));
        assertFalse(luhn.validateCreditCard("45654585965845345646"));
    }

    @Test
    void tooHigh(){
        assertTrue(luhn.tooHigh(16));
        assertTrue(luhn.tooHigh(10));
        assertFalse(luhn.tooHigh(9));
        assertFalse(luhn.tooHigh(5));
    }

    @Test
    void doubleDigit(){
        assertEquals(4, luhn.doubleDigit(2));
        assertEquals(18, luhn.doubleDigit(9));
        assertNotEquals(5, luhn.doubleDigit(2));
        assertNotEquals(12, luhn.doubleDigit(5));
    }
}