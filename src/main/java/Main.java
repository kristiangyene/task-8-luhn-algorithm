import algorithms.LuhnAlgorithm;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Variables.
        LuhnAlgorithm luhn = new LuhnAlgorithm();
        String digits = requestDigits();
        String lastDigit = digits.substring(digits.length() - 1);
        String firstDigits = digits.substring(0, digits.length() - 1);
        String expected = luhn.calculateLuhn(firstDigits);

        //Console prints.
        System.out.println("Input: " + firstDigits + " " + lastDigit);
        System.out.println("Provided: " + lastDigit);
        System.out.println("Expected: " + expected);
        System.out.println("Checksum: " + luhn.checkSum(expected, lastDigit));
        if(luhn.validateCreditCard(digits)) System.out.println("Digits: " + digits.length() + " (credit card)");
        else System.out.println("Digits: " + digits.length() + " (not a credit card)");
    }


    public static String requestDigits(){
        // Request digits from user to validate.
        Scanner scanner = new Scanner(System.in);
        System.out.print("Write digits with ant length:\n> ");
        String digits = "";
        try{
            digits = scanner.nextLine();
        }catch (NoSuchElementException e){
            System.err.println(e);
        }
        return digits;
    }
}
