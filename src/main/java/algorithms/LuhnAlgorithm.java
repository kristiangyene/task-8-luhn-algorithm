package algorithms;

public class LuhnAlgorithm {

    public LuhnAlgorithm() {
    }

    public String checkSum(String expected, String provided){
        // Check if the last digit matches with the user digit.
        if(!expected.equals(provided)) return "Not valid";
        return "Valid";
    }


    public String calculateLuhn(String digits){
        char[] digitList = digits.toCharArray();
        int total = 0;
        boolean skip = false;
        // Loop through digits, starting from the right.
        for(int i = (digits.length()-1); i >= 0; i--){
            int digit = Integer.parseInt(String.valueOf(digitList[i]));
            if(!skip){
                digit = doubleDigit(digit); // Double every second digit.
                if(tooHigh(digit)){ // Keep the numbers below 10.
                    digit -= 9;
                }
                skip = true; // Skip every second digit.
            }
            else skip = false;
            total += digit;
        }
        return Integer.toString((total * 9) % 10);
    }

    public int doubleDigit(int digit){
        return digit * 2;
    }

    public Boolean tooHigh(int digit){
        return digit > 9;
    }

    public Boolean validateCreditCard(String digits){
        // Check if it is a credit card or not.
        int numberOfDigits = digits.length();
        return numberOfDigits == 16;
    }
}
